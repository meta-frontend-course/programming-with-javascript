function finalPrice(dishes, withTax) {
    if (withTax) {
        console.log('Prices with 20% tax:')
        for (dish of dishes) {
            console.log(`Dish: ${dish.menu} Price (incl.tax): $${(dish.price + dish.price*0.2).toFixed(2)}`)
        }
    } else {
        console.log('Prices without tax:')
        for (dish of dishes) {
            console.log(`Dish: ${dish.menu} Price (incl.tax): $${dish.price}`)
        }
    }
}

const dishes = [
    {
        menu: 'Italian pasta',
        price: 9.55
    },
    {
        menu: 'Rice with veggies',
        price: 8.65
    },
    {
        menu: 'Chicken with potatoes',
        price: 15.55
    },
    {
        menu: 'Vegetarian Pizza',
        price: 6.45
    },
]

finalPrice(dishes, true)
console.log()
finalPrice(dishes, false)



var a
console.log(a)
a = 10